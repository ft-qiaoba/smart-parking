# Smart-Parking

#### 介绍

基于 SpringBoot +mybatis-plus + HTML5 +bootstrap 的停车场管理系统项目

#### 基础环境

JDK1.8、Maven、Mysql、IntelliJ IDEA

#### 相关组件

- [ok-admin](https://gitee.com/bobi1234/ok-admin)
- [vue](https://cn.vuejs.org/)
- [iView](http://v1.iviewui.com/)
- [echarts](https://echarts.apache.org/zh/index.html)
- clipboard
- cropperjs
- lightbox
- nprogress
- webuploader
- ztree

#### 内置功能

- 系统管理：角色管理、接口管理、系统菜单、全局配置

- 账号管理：用户管理、合作单位

- 系统监控：监控大屏、日志监控

- 财务管理：订单列表

- 停车记录：停车记录

- 车辆管理：车辆管理

- 车牌识别：车牌识别

- 停车场管理：停车场管理




#### 安装教程

- 启动前请配置 `application-dev.properties` 中相关`mysql` 

- 数据库脚本启动前请自行导入。


- 配置完成，运行`Application`中的 `main `方法。

- 测试账号：admin 密码：admin


#### 演示图


<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0627/143152_4666a41a_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0627/143228_2826d431_87650.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0627/143257_6dcf0f44_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0627/143427_cf1d4c3f_87650.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0627/143450_688aef23_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0627/143510_7737027f_87650.png"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0627/143532_e108417b_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0627/143613_a65ca0da_87650.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0627/143701_9c521618_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0627/143742_7c0baaf2_87650.png"/></td>
    </tr>

 </tr>

</table>


作者：涛哥



